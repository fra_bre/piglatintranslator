package piglatintranslator;

public class Translator {
	
	public static final String NIL = "nil";
	public static final String[] VOWELS = { "a", "e", "i", "o", "u", "A", "E", "I", "O", "U" };
	public static final int NUMBEROFVOWELS = 10;
	
	private String phrase;

	public Translator(String inputPhrase) {
		phrase = inputPhrase;
	}

	public String getPhrase() {
		return phrase;
	}

	public String translate() {
		int typeCaseForWord;
		String returnValue = "";
		
		if(!phrase.isEmpty()) {
			typeCaseForWord = typeOfCase();
			phrase = phrase.toLowerCase();
			if(startWithVowel()) {
				if(phrase.endsWith("y") || phrase.endsWith("Y")) {
					returnValue = phrase + "nay";
				}else if(endWithVowel()) {
					returnValue = phrase + "yay";
				}else if(!endWithVowel()) {
					returnValue = phrase + "ay";
				}
			}else if(!startWithVowel() && (!phrase.isEmpty())) {
				for(int i=0; i<(phrase.length()-1) && (!charIsVowel(phrase.charAt(0))); i++) {
					phrase += phrase.substring(0, 1);
					phrase = moveCharLeft();
				}
				returnValue = phrase + "ay";
			}
			if(typeCaseForWord == 1)
				return returnValue.toUpperCase();
			else if(typeCaseForWord == 0)
				return toTitleCase(returnValue);
			else
				return returnValue;
		}
		return NIL;
	}
	
	private boolean startWithVowel() {
		boolean returnValue = false;
		
		for(int i=0; i<NUMBEROFVOWELS; i++)
			if(phrase.startsWith(VOWELS[i]))
				returnValue = true;
		return returnValue;
	}
	
	private boolean endWithVowel() {
		boolean returnValue = false;
		
		for(int i=0; i<NUMBEROFVOWELS; i++)
			if(phrase.endsWith(VOWELS[i]))
				returnValue = true;
		return returnValue;
	}

	private boolean charIsVowel(char character) {
		boolean returnValue = false;
		
		for(int i=0; i<NUMBEROFVOWELS; i++)
			if(character == VOWELS[i].charAt(0))
				returnValue = true;
		return returnValue;
	}
	
	private String moveCharLeft () {	
		String temporaryPhrase = "";
		
		for(int i=0; i<(phrase.length()-1); i++) {
			temporaryPhrase += phrase.substring(i+1, i+2);
		}
		return temporaryPhrase;
	}
	
	private int typeOfCase() {
		int typeCase = -1;
		
		if(Character.isUpperCase(phrase.charAt(0))) {
			if(Character.isLowerCase(phrase.charAt(1)))
				typeCase = 0;
			else
				typeCase = 1;
		}
		
		return typeCase;
	}

	public String translatePhrase() {
		String inputPhrase = phrase;
		String returnPhrase = "";
		String temporaryWord = "";
		
		for(int i=0; i<phrase.length(); i++) {
			if(!isPunctuationOrSpaceOrLine(phrase.substring(i, i+1)))
				temporaryWord += phrase.substring(i, i+1);
			else {
				if(!temporaryWord.equals("")) {
					phrase = temporaryWord;
					returnPhrase += translate();
				}
				phrase = inputPhrase;
				returnPhrase += phrase.substring(i, i+1);
				temporaryWord = "";
			}	
		}
		if(!temporaryWord.equals("")) {
			phrase = temporaryWord;
			returnPhrase += translate();
		}
	
		return returnPhrase;
	}
	
	private boolean isPunctuationOrSpaceOrLine (String punctuation) {
		boolean returnValue = false;
		
		if (punctuation.equals(" ")) {
			returnValue = true;
		}else if (punctuation.equals("-")) {
			returnValue = true;
		}else if (punctuation.equals("?")) {
			returnValue = true;
		}else if (punctuation.equals("!")) {
			returnValue = true;
		}else if (punctuation.equals("(")) {
			returnValue = true;
		}else if (punctuation.equals(")")) {
			returnValue = true;
		}else if (punctuation.equals(",")) {
			returnValue = true;
		}else if (punctuation.equals(".")) {
			returnValue = true;
		}else if (punctuation.equals(":")) {
			returnValue = true;
		}else if (punctuation.equals(";")) {
			returnValue = true;
		}else if (punctuation.equals("'")) {
			returnValue = true;
		}
		
		return returnValue;
	}
	
	private String toTitleCase(String inputPhrase) {
		String returnValue = inputPhrase.toUpperCase().substring(0,1);
		
		for(int i=1; i<inputPhrase.length(); i++) {
			if(!isPunctuationOrSpaceOrLine(inputPhrase.substring(i, i+1)))
				returnValue += inputPhrase.substring(i, i+1);
			else {
				returnValue += inputPhrase.substring(i, i+1);
				i++;
				if(i<inputPhrase.length()) 
					returnValue += inputPhrase.toUpperCase().substring(i, i+1);
			}	
		}
		
		return returnValue;
	}
}

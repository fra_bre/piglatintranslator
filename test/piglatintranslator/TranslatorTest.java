package piglatintranslator;

import static org.junit.Assert.*;

import org.junit.Test;

public class TranslatorTest {

	@Test
	public void testInputPhrase() {
		String inputPhrase = "hello world";
		Translator translator = new Translator(inputPhrase);
		assertEquals("hello world", translator.getPhrase());
	}
	
	@Test
	public void testTranslationEmptyPhrase() {
		String inputPhrase = "";
		Translator translator = new Translator(inputPhrase);
		assertEquals(Translator.NIL, translator.translate());
	}
	
	@Test
	public void testTranslationPhraseStartingWithAEndingWithY() {
		String inputPhrase = "any";
		Translator translator = new Translator(inputPhrase);
		assertEquals("anynay", translator.translate());
	}
	
	@Test
	public void testTranslationPhraseStartingWithUEndingWithY() {
		String inputPhrase = "utility";
		Translator translator = new Translator(inputPhrase);
		assertEquals("utilitynay", translator.translate());
	}
	
	@Test
	public void testTranslationPhraseStartingWithVowelEndingWithVowel() {
		String inputPhrase = "apple";
		Translator translator = new Translator(inputPhrase);
		assertEquals("appleyay", translator.translate());
	}
	
	@Test
	public void testTranslationPhraseStartingWithVowelEndingWithConsonant() {
		String inputPhrase = "ask";
		Translator translator = new Translator(inputPhrase);
		assertEquals("askay", translator.translate());
	}
	
	@Test
	public void testTranslationPhraseStartingWithSingleConsonant() {
		String inputPhrase = "hello";
		Translator translator = new Translator(inputPhrase);
		assertEquals("ellohay", translator.translate());
	}
	
	@Test
	public void testTranslationPhraseStartingWithSingleConsonantSecondTest() {
		String inputPhrase = "come";
		Translator translator = new Translator(inputPhrase);
		assertEquals("omecay", translator.translate());
	}

	@Test
	public void testTranslationPhraseKnown() {
		String inputPhrase = "known";
		Translator translator = new Translator(inputPhrase);
		assertEquals("ownknay", translator.translate());
	}
	
	@Test
	public void testTranslationPhraseStartingWith2Consonant() {
		String inputPhrase = "knee";
		Translator translator = new Translator(inputPhrase);
		assertEquals("eeknay", translator.translate());
	}
	
	@Test
	public void testTranslationPhraseStartingWithMoreConsonant() {
		String inputPhrase = "system";
		Translator translator = new Translator(inputPhrase);
		assertEquals("emsystay", translator.translate());
	}
	
	@Test
	public void testTranslationPhraseHelloWorld() {
		String inputPhrase = "hello world";
		Translator translator = new Translator(inputPhrase);
		assertEquals("ellohay orldway", translator.translatePhrase());
	}
	
	@Test
	public void testTranslationPhraseWithMoreWordsSeparatedBySpace() {
		String inputPhrase = "fine thanks";
		Translator translator = new Translator(inputPhrase);
		assertEquals("inefay anksthay", translator.translatePhrase());
	}
	
	@Test
	public void testTranslationPhraseWithMoreWordsSeparatedByLine() {
		String inputPhrase = "well-being";
		Translator translator = new Translator(inputPhrase);
		assertEquals("ellway-eingbay", translator.translatePhrase());
	}
	
	@Test
	public void testTranslationPhraseWithMoreWordsAndExclamationMark() {
		String inputPhrase = "hello world!";
		Translator translator = new Translator(inputPhrase);
		assertEquals("ellohay orldway!", translator.translatePhrase());
	}
	
	@Test
	public void testTranslationPhraseWithMoreWordsAndPunctuations() {
		String inputPhrase = "where is bug?";
		Translator translator = new Translator(inputPhrase);
		assertEquals("erewhay isay ugbay?", translator.translatePhrase());
	}
	
	@Test
	public void testTranslationPhraseWithMoreWordsAndPunctuationsSecondTest() {
		String inputPhrase = "(remove the consonant)";
		Translator translator = new Translator(inputPhrase);
		assertEquals("(emoveray ethay onsonantcay)", translator.translatePhrase());
	}
	
	@Test
	public void testTranslationPhraseWithMoreWordsAndPunctuationsThirdTest() {
		String inputPhrase = " remove, the consonant.";
		Translator translator = new Translator(inputPhrase);
		assertEquals(" emoveray, ethay onsonantcay.", translator.translatePhrase());
	}
	
	@Test
	public void testTranslationPhraseAppleWithUpperCase() {
		String inputPhrase = "APPLE";
		Translator translator = new Translator(inputPhrase);
		assertEquals("APPLEYAY", translator.translatePhrase());
	}
	
	@Test
	public void testTranslationPhraseSystemWithUpperCase() {
		String inputPhrase = "SYSTEM";
		Translator translator = new Translator(inputPhrase);
		assertEquals("EMSYSTAY", translator.translatePhrase());
	}
	
	@Test
	public void testTranslationPhraseKnownWithUpperCase() {
		String inputPhrase = "KNOWN";
		Translator translator = new Translator(inputPhrase);
		assertEquals("OWNKNAY", translator.translatePhrase());
	}
	
	@Test
	public void testTranslationPhraseWithUpperCase() {
		String inputPhrase = "ANY";
		Translator translator = new Translator(inputPhrase);
		assertEquals("ANYNAY", translator.translatePhrase());
	}
	
	@Test
	public void testTranslationPhraseWithTitleCase() {
		String inputPhrase = "Hello";
		Translator translator = new Translator(inputPhrase);
		assertEquals("Ellohay", translator.translatePhrase());
	}
	
	@Test
	public void testTranslationPhraseWithMoreWordsAndPunctuationsTitleCase() {
		String inputPhrase = " Remove, The Consonant.";
		Translator translator = new Translator(inputPhrase);
		assertEquals(" Emoveray, Ethay Onsonantcay.", translator.translatePhrase());
	}
	
	@Test
	public void testTranslationPhraseWithMoreWordsAndPunctuationsUpperCase() {
		String inputPhrase = "(REMOVE THE CONSONANT) ";
		Translator translator = new Translator(inputPhrase);
		assertEquals("(EMOVERAY ETHAY ONSONANTCAY) ", translator.translatePhrase());
	}
}
